﻿pycbg.MPMxDEM.DefineCallable
============================

.. currentmodule:: pycbg.MPMxDEM

.. autoclass:: DefineCallable
   :members:

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
      :nosignatures:
   
      ~DefineCallable.__init__
      ~DefineCallable.run_dem_steps_fdt
      ~DefineCallable.run_dem_steps_fsr
   
   

   
   
   