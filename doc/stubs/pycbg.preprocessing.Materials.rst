﻿pycbg.preprocessing.Materials
=============================

.. currentmodule:: pycbg.preprocessing

.. autoclass:: Materials
   :members:

   
   .. automethod:: __init__

   
   .. rubric:: Methods

   .. autosummary::
      :nosignatures:
   
      ~Materials.__init__
      ~Materials.create_CustomLaw
      ~Materials.create_LinearElastic
      ~Materials.create_MohrCoulomb
      ~Materials.create_Newtonian
      ~Materials.create_NorSand
      ~Materials.create_PythonModel
   
   

   
   
   