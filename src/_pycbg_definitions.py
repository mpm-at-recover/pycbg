import os

# Path to PyCBG root directory
SRC_DIR = os.path.dirname(os.path.abspath(__file__))

# Path to the project directory
PROJ_DIR = SRC_DIR.rsplit("/", 1)[0]

# Path to the test directory
TESTS_DIR = f"{SRC_DIR}/tests"

# Path to the build_doc.sh script
BUILD_DOC_SCRIPT = f"{PROJ_DIR}/build_doc.sh"