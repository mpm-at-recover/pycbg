#!/bin/bash

# Determine the unique name of the slot file
slot_file_name="/tmp/n_sim_slot_available_$(uuidgen).sh"

# Function to handle cleanup when SIGINT or SIGTERM is received
cleanup() {
    local signal="$1"
    echo -e "\n\e[91m$(date +'[%Y-%m-%d %T.%N]')\e[0m\tReceived $signal, terminating..."
    
    # Kill all child processes
    pkill -P $$

    # Wait for child processes to exit
    wait

    # Remove slot file
    rm $slot_file_name

    # Exit the script
    exit 1
}

# Function to check if simulation duration is greater than 24 hours
format_duration() {
    local t_sim=$1
    local days_sim=""
    if test $t_sim -gt $(((3600*24))); then
        days_sim="%jd-"
        t_sim=$((t_sim-3600*24))
    else
        days_sim=""
    fi
    echo $(date -u --date @$t_sim +$days_sim%H:%M:%S)
}

# Trap SIGINT and SIGTERM signals and call the cleanup function
trap 'cleanup "SIGINT"' SIGINT
trap 'cleanup "SIGTERM"' SIGTERM

# Define default values for optional parameters
resume_from=""
executable="##EXECUTABLE##"

# Check if optional parameters were provided
while [[ $# -gt 0 ]]; do
    key="$1"

    case $key in
        -r|--resume-from)
            resume_from="$2"
            shift # past argument
            shift # past value
            ;;
        -e|--executable)
            executable="$2"
            shift # past argument
            shift # past value
            ;;
        *)    # unknown option
            shift # past argument
            ;;
    esac
done

# Define batch variables
batch_dir=##BATCH_DIR##
nthreads=##NTHREADS##
nthreads_each_job=(##NTHREADS_EACH_JOB##)
sims_ids=(##SIMS_IDS##)
python_run_str="##PYTHON_RUN_STR##"

# Define the number of simulation to be run at the same time, and write it to a temporary file for sharing accross all subprocesses
n_sim_slot_available=##NSLOTS##
echo "n_sim_slot_available=$n_sim_slot_available" > $slot_file_name

# Check if the user wants the batch to be run sequentially
if [ "$nthreads" -gt 0 ]; then
    n_thread_used_str="Maximum number of threads to be used: ${nthreads}"
else
    n_thread_used_str="Running all simulations sequentially"
fi

# Display useful information
echo -e "\e[90m$(date +'[%Y-%m-%d %T.%N]')\e[0m\tStarting batch from directory $batch_dir\n\n"
echo -e "\tPyCBG version: ##PYCBG_VERSION##\n\t${n_thread_used_str}\n\n"

# Start timer for measuring the batch's duration
startT=$(date +%s)

# Loop over all simulations
resume_flag=$([ -n "$resume_from" ] && echo true || echo false) # Check if the batch has to be resumed from a specific simulation
for index in "${!sims_ids[@]}"; do
    # Get parameters for this simulation
    sim_id="${sims_ids[index]}"
    nthreads_job="${nthreads_each_job[index]}"

    # Construct simulation name
    sim="sim${sim_id}" 

    # Check if the simulation has to be skipped because resume_from was set
    if [ "$resume_flag" == true ]; then
        if [ "$sim" != "$resume_from" ]; then
            continue
        else 
            echo -e "\e[90m$(date +'[%Y-%m-%d %T.%N]')\e[0m\tResuming from ${sim}..."
            resume_flag=false
        fi
    fi

    # Check if a CPU core offset has to be used
    if [ -z "$python_run_str" ]; then
        offset_str=""
    else
        offset="${python_run_str[index]}"
        offset_str=" -o ${offset}"
    fi
    
    # Wait until a slot is available
    until [ "$n_sim_slot_available" -gt "0" ]; do
        sleep 0.1
        # Get the updated number of slots available
        . $slot_file_name
    done

    # Decrease the number of slot available, and write it to the temporary file so other processes are aware of its updated value
    n_sim_slot_available=$((--n_sim_slot_available))
    echo "n_sim_slot_available=$n_sim_slot_available" > $slot_file_name
    
    # Start subprocess that runs the simulation
    (
        # Start timer for measuring the simulation's duration
    startT_sim=$(date +%s)

        # Display useful information
    echo -e "\e[90m$(date +'[%Y-%m-%d %T.%N]')\e[0m\tStarting ${sim} using ${nthreads_job} threads;\tlog file: ${batch_dir}${sim}/cbgeo.log"

        # Start the simulation
    ${executable} -p ${nthreads_job}${offset_str} -f ./ -i "${batch_dir}${sim}/input_file.json" &> "${batch_dir}${sim}/cbgeo.log"
    t_sim=$(( $(date +%s) - $startT_sim ))

        # Display useful information
    echo -e "\e[90m$(date +'[%Y-%m-%d %T.%N]')\e[0m\tFinished ${sim};\tduration: $(format_duration $t_sim)"
        # Get the updated number of slots available
    . $slot_file_name

        # Increase the number of slots available, as one just has been freed
    n_sim_slot_available=$((++n_sim_slot_available))
    echo "n_sim_slot_available=$n_sim_slot_available" > $slot_file_name
    ) &
done

wait

# Stop the batch timer and display useful information
t_all=$(( $(date +%s) - $startT ))
echo -ne "\n\e[90m$(date +'[%Y-%m-%d %T.%N]')\e[0m\tBatch over, total duration: $(format_duration $t_all)\n"

# Remove slot file
rm $slot_file_name
